from cv2 import cv2
import os
import numpy as np
import datetime
from skimage.measure import compare_ssim
import json
import argparse
import re
import pyocr
import imutils
from pyocr import pyocr as po
import sys
import pytesseract
from PIL import Image

# Dimensions for the barcode RoI and the digits needed for validations
BarcodesDim = [0.07,0.2539181286549708,0.0853333333333333,0.9233870967741935]
DigitsDim = [0.0,0.4827586206896552,0.0,1]
LastThreeDim = [0.0,0.99,0.7647479674796748,0.9834146341463415]
DateCheckDim = [0.01,0.99,0.3124339622641509,0.6382113821138211]

outputs = ['images/Dates/', 'images/LastThree/']
ImgsPath = 'start/'

# loop through the template images and creqate a list of their directories
def get_templates():
    for file in os.listdir(ImgsPath):
        if file.endswith(".png"):
            files.append(os.path.join(ImgsPath, file))

def Date_Nationality_Validation(image, DoBimage, Type):
    #Extract and Write Barcodes
    (h, w) = image.shape[:2]
    BarcodePicOrig = image[round(int(h)*BarcodesDim[0]) : round(int(h)*BarcodesDim[1]), round(int(w)*BarcodesDim[2]) : round(int(w)*BarcodesDim[3])]
    BarcodePic = Barcode_Thresh(BarcodePicOrig)
    (h2, w2) = BarcodePic.shape[:2]
    DigitsPic = BarcodePic[round(int(h2)*DigitsDim[0]) : round(int(h2)*DigitsDim[1]), round(int(w2)*DigitsDim[2]) : round(int(w2)*DigitsDim[3])]
    (h3, w3) = DigitsPic.shape[:2]
    LastThreePic = DigitsPic[round(int(h3)*LastThreeDim[0]) : round(int(h3)*LastThreeDim[1]), round(int(w3)*LastThreeDim[2]) : round(int(w3)*LastThreeDim[3])]
    DateCheckPic = DigitsPic[round(int(h3)*DateCheckDim[0]) : round(int(h3)*DateCheckDim[1]), round(int(w3)*DateCheckDim[2]) : round(int(w3)*DateCheckDim[3])]
    cv2.imwrite(outputs[1] + 'str(count).png', LastThreePic)
    cv2.imwrite(outputs[0] + 'str(count).png', DateCheckpic)

for file in files:
    Date_Nationality_Validation
