# USAGE
# python3 ID_Validate_batch.py

from digit_extraction import digit_extraction_full as digits_extract
from cv2 import cv2
import os
import numpy as np
import datetime
from skimage.measure import compare_ssim
import json
import re
import imutils
import sys
from PIL import Image

# Dimensions of the various RoIs in the ID book
RoIDim = [0.5571345029239766,0.7994736842105263,0.4538709677419355,0.9693548387096774]
CoBDim = [[0.4898854568854569,0.584015444015444,0.0957446808510638,0.6748387096774194],[0.5225102319236016,0.5907230559345157,0.0777446808510638,0.6748387096774194]]
DoIDim = [[0.6471345029239766,0.7594736842105263,0.5338709677419355,0.9093548387096774],[0.6971350613915416,0.7667121418826739,0.5038709677419355,0.8893548387096774]]
DoBDim = [[0.5644015444015444,0.6625146198830409,0.0957446808510638,0.4553896832084727],[0.5675306957708049,0.6493860845839018,0.5038709677419355,0.8893548387096774]]
NameDim = [[0.3559073359073359,0.4782754182754183,0.0957446808510638,0.9448387096774194],[0.3410641200545703,0.461118690313779,0.0777446808510638,0.9448387096774194]]
SurnameDim = [[0.254826254826254,0.3538011695906433,0.0797872340425532,0.9448387096774194],[0.2223738062755798,0.3410641200545703,0.0777872340425532,0.9448387096774194]]

# Dimensions for the barcode RoI and the digits needed for validations
BarcodesDim = [[0.07,0.2539181286549708,0.0853333333333333,0.9233870967741935],[0.02,0.2439181286549708,0.0813333333333333,0.9833870967741935]]
DigitsDim = [[0.0,0.4827586206896552,0.0,1],[0.0,0.5857586206896552,0.0,1]]
LastThreeDim = [[0.0,0.99,0.7647479674796748,0.9834146341463415],[0.0,0.99,0.6747479674796748,0.9834146341463415]]
DateCheckDim = [[0.01,0.99,0.3124339622641509,0.6382113821138211],[0.01,1,0.2094339622641509,0.5582113821138211]]

# Dimensions for the 4 Emblem image areas on the ID Book
Emblem1Dim = [0.2148543689320388,0.3948543689320388,0.5040136054421769,0.7440136054421769]
Emblem2Dim = [0.4454368932038835,0.6254368932038835,0.5924489795918367,0.8324489795918367]
Emblem3Dim = [0.7925242718446602,0.9725242718446602,0.3577551020408163,0.5977551020408163]
Emblem4Dim = [0.5179611650485437,0.7179611650485437,0.0928571428571429,0.3528571428571429]

# Create Variables for the paths and image areas
Emblempath = 'Templates/Emblem.png'
Regions = ['CoB', 'DoI', 'DoB', 'Name', 'Sur']

# Create the colour boundaries for the RoIs to find data
emblem_boundaries = [([123,163,15],[160,190,115])]
boundaries = [([0,0,0],[123,135,123])]
barcode_boundaries= [([0,0,0],[156,156,156])]

# Functionality for extracting RoIs from the ID Book
def Extract_ROIs(image, Type):
    # Get ID width and height
    (h, w) = image.shape[:2]

    #Extract and Write CoB
    CoBPic = image[round(int(h)*CoBDim[Type][0]) : round(int(h)*CoBDim[Type][1]), round(int(w)*CoBDim[Type][2]) : round(int(w)*CoBDim[Type][3])]

    #Extract and Write DoIs
    DoIPic = image[round(int(h)*DoIDim[Type][0]) : round(int(h)*DoIDim[Type][1]), round(int(w)*DoIDim[Type][2]) : round(int(w)*DoIDim[Type][3])]

    #Extract and Write DoBs
    DoBPic = image[round(int(h)*DoBDim[Type][0]) : round(int(h)*DoBDim[Type][1]), round(int(w)*DoBDim[Type][2]) : round(int(w)*DoBDim[Type][3])]

    #Extract and Write Names
    NamePic = image[round(int(h)*NameDim[Type][0]) : round(int(h)*NameDim[Type][1]), round(int(w)*NameDim[Type][2]) : round(int(w)*NameDim[Type][3])]

    #Extract and Write SurNames
    SurnamePic = image[round(int(h)*SurnameDim[Type][0]) : round(int(h)*SurnameDim[Type][1]), round(int(w)*SurnameDim[Type][2]) : round(int(w)*SurnameDim[Type][3])]
    
    ROIs = [CoBPic, DoIPic, DoBPic, NamePic, SurnamePic]

    return ROIs

# Functionality for threshholding RoIs from the ID Book
def ROI_Thresh(ROIs, Type):
    for file in ROIs:
        imgLoad = file
        for (lower, upper) in boundaries:
            # create NumPy arrays from the boundaries
            lower = np.array(lower, dtype = 'uint8')
            upper = np.array(upper, dtype = 'uint8')

            (h,w) = imgLoad.shape[:2]
            blank_image = np.zeros((h,w,3), np.uint8)

            # find the colors within the specified boundaries and apply the mask
            mask = cv2.inRange(imgLoad, lower, upper)
            img = cv2.bitwise_not(imgLoad, blank_image, mask = mask)

            ret, img = cv2.threshold(img, 0, 255, cv2.THRESH_BINARY_INV)

        Threshed.append(img)
    return Threshed

# Functionality for finding the emblems and counting how many correct matches in the ID Book
def find_and_validate_emblems(image):
    totalFoundCount = 0
    EmblemPics = []

    img = image
    (h, w) = img.shape[:2]

    EmblemPics.append(img[round(int(h)*Emblem1Dim[0]) : round(int(h)*Emblem1Dim[1]), round(int(w)*Emblem1Dim[2]) : round(int(w)*Emblem1Dim[3])])
    EmblemPics.append(img[round(int(h)*Emblem2Dim[0]) : round(int(h)*Emblem2Dim[1]), round(int(w)*Emblem2Dim[2]) : round(int(w)*Emblem2Dim[3])])
    EmblemPics.append(img[round(int(h)*Emblem3Dim[0]) : round(int(h)*Emblem3Dim[1]), round(int(w)*Emblem3Dim[2]) : round(int(w)*Emblem3Dim[3])])
    EmblemPics.append(img[round(int(h)*Emblem4Dim[0]) : round(int(h)*Emblem4Dim[1]), round(int(w)*Emblem4Dim[2]) : round(int(w)*Emblem4Dim[3])])

    for img in EmblemPics:

        found = [0]
        foundCount = 0

        for (lower, upper) in emblem_boundaries:
            # create NumPy arrays from the boundaries
            lower = np.array(lower, dtype = 'uint8')
            upper = np.array(upper, dtype = 'uint8')

            # find the colors within the specified boundaries and apply the mask
            mask = cv2.inRange(img, lower, upper)
            img = cv2.bitwise_and(img, img, mask = mask)

            kernel = np.ones((2,2),np.uint8)
            img = cv2.dilate(img,kernel,iterations = 1)

            img = cv2.bitwise_not(img)
            ret, img = cv2.threshold(img, 120, 255, cv2.THRESH_BINARY)
            img = cv2.bitwise_not(img)
            
        #Get image dimensions for template resizing and get canny edges for ROI in image
        (w, h) = img.shape[:2]
        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        img = cv2.Canny(img, 50, 200)

        #Template read and manipulate to fit Image dimensions and get canny edges of template image
        template = cv2.imread(Emblempath)
        width = round(w*0.3)
        height = round(h*0.5)
        template = cv2.resize(template, (width, height))
        template = cv2.cvtColor(template, cv2.COLOR_BGR2GRAY)
        template = cv2.Canny(template, 50, 200)
        (tH, tW) = template.shape[:2]

        for scale in np.linspace(0.2, 1.0, 20)[::-1]:
            # resize the image according to the scale, and keep track of the ratio of the resizing
            resized = imutils.resize(img, width = int(img.shape[1] * scale))
            r = img.shape[1] / float(resized.shape[1])

            # if the resized image is smaller than the template, then break from the loop
            if resized.shape[0] < tH or resized.shape[1] < tW:
                break

            # detect edges in the resized, grayscale image and apply template matching to find the template in the image
            result = cv2.matchTemplate(resized, template, cv2.TM_CCOEFF)
            (_, maxVal, _, maxLoc) = cv2.minMaxLoc(result)

            # if we have found a new maximum correlation value, then update the bookkeeping variable
            if found is None or maxVal > found[0]:
                found = (maxVal, maxLoc, r)

        # unpack the bookkeeping varaible and compute the (x, y) coordinates of the bounding box based on the resized ratio
        if found[0] > 850000:
            foundCount = foundCount + 1

        totalFoundCount = totalFoundCount + foundCount
    return totalFoundCount

# Functionality for validating the dates and numbers within the ID Book
def Date_Nationality_Validation(image, DoBimage, Type):
    #Extract all the RoIs from the Barcode
    (h, w) = image.shape[:2]
    BarcodePicOrig = image[round(int(h)*BarcodesDim[Type][0]) : round(int(h)*BarcodesDim[Type][1]), round(int(w)*BarcodesDim[Type][2]) : round(int(w)*BarcodesDim[Type][3])]
    BarcodePic = Barcode_Thresh(BarcodePicOrig)
    (h2, w2) = BarcodePic.shape[:2]
    DigitsPic = BarcodePic[round(int(h2)*DigitsDim[Type][0]) : round(int(h2)*DigitsDim[Type][1]), round(int(w2)*DigitsDim[Type][2]) : round(int(w2)*DigitsDim[Type][3])]
    (h3, w3) = DigitsPic.shape[:2]
    LastThreePic = DigitsPic[round(int(h3)*LastThreeDim[Type][0]) : round(int(h3)*LastThreeDim[Type][1]), round(int(w3)*LastThreeDim[Type][2]) : round(int(w3)*LastThreeDim[Type][3])]
    DateCheckPic = DigitsPic[round(int(h3)*DateCheckDim[Type][0]) : round(int(h3)*DateCheckDim[Type][1]), round(int(w3)*DateCheckDim[Type][2]) : round(int(w3)*DateCheckDim[Type][3])]
    
    # View All the RoIs extracted from the barcode
    # cv2.imshow('BarOrig', BarcodePicOrig)
    # cv2.imshow('Bar', BarcodePic)
    # cv2.imshow('DigPic', DigitsPic)
    # cv2.imshow('LastThree', LastThreePic)
    # cv2.imshow('DateCheck', DateCheckPic)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()
    
    # Get the DoB from the ID and the Barcode areas
    barDoB = barDoB_Fetch(DateCheckPic, Type)
    DoB = DoB_Fetch(DoBimage, Type)
    
    # In case the barcode Date of Birth cannot be read, try
    if barDoB == 'False':
        DigitsPic = BarcodePicOrig[round(int(h2)*DigitsDim[Type][0]) : round(int(h2)*DigitsDim[Type][1]), round(int(w2)*DigitsDim[Type][2]) : round(int(w2)*DigitsDim[Type][3])]
        DateCheckPic = DigitsPic[round(int(h3)*DateCheckDim[Type][0]) : round(int(h3)*DateCheckDim[Type][1]), round(int(w3)*DateCheckDim[Type][2]) : round(int(w3)*DateCheckDim[Type][3])]
        barDoB = barDoB_Fetch(DateCheckPic, Type)
        if barDoB == 'False':
            DateCheck = 'False'
    # If either DoB cannot be read, don't bother about the date check method
    if barDoB == 'False' or DoB == 'False':
        DateCheck = 'False'
    # If both contain digits, perform the date check
    else:
        DateCheck = date_check(barDoB, DoB)

    LastThreeResult = LastThree_Validate(LastThreePic, Type)
    if LastThreeResult == 'Could not Match':
        DigitsPic = BarcodePicOrig[round(int(h2)*DigitsDim[Type][0]) : round(int(h2)*DigitsDim[Type][1]), round(int(w2)*DigitsDim[Type][2]) : round(int(w2)*DigitsDim[Type][3])]
        LastThreePic = DigitsPic[round(int(h3)*LastThreeDim[Type][0]) : round(int(h3)*LastThreeDim[Type][1]), round(int(w3)*LastThreeDim[Type][2]) : round(int(w3)*LastThreeDim[Type][3])]
        LastThreeResult = LastThree_Validate(LastThreePic, Type)

    return BarcodePicOrig, DateCheck, LastThreeResult

def Barcode_Thresh(img):
    for (lower, upper) in barcode_boundaries:   
        lower = np.array(lower, dtype = 'uint8')
        upper = np.array(upper, dtype = 'uint8')

        (h,w) = img.shape[:2]
        blank_image = np.zeros((h,w,3), np.uint8)

        # find the colors within the specified boundaries and apply the mask
        mask = cv2.inRange(img, lower, upper)
        img = cv2.bitwise_not(img, blank_image, mask = mask)

        ret, img = cv2.threshold(img, 0, 255, cv2.THRESH_BINARY_INV)
    return img

# Functionality for extracting the digits from the barcode DoB area
def barDoB_Fetch(barDoB, type):
    barcodeTrue = 'True'

    # cv2.imshow('img', barDoB)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()

    digits = digits_extract(barDoB, type)
    # find length of digits string
    length = len(digits)
    # Move onto the next one if cannot find any digits
    if length == 0 or length == 1:
        barcodeTrue = 'False'
        return barcodeTrue

    #Make sure that we are focusing on the right second last digit
    length = len(digits)
    if length > 6:
        digits = digits[:6]

    if length < 6:
        barcodeTrue = 'False'

    if barcodeTrue != 'False':
        if digits[0] == '0':
            digits = '20' + digits[0] + digits[1] + '-' + digits[2] + digits[3] + '-' + digits[4] + digits[5]
            barcodeTrue = digits
            return barcodeTrue
        else:
            digits = '19' + digits[0] + digits[1] + '-' + digits[2] + digits[3] + '-' + digits[4] + digits[5]
            barcodeTrue = digits
            return barcodeTrue

    return barcodeTrue

# Functionality for extracting the digits from the DoB area
def DoB_Fetch(DoB, type):
    DateTrue = 'True'

    # cv2.imshow('img', DoB)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()

    digits = digits_extract(DoB, type)
    # find length of digits string
    length = len(digits)
    # Move onto the next one if cannot find any digits
    if length == 0 or length == 1:
        DateTrue = 'False'
        return DateTrue

    digits = digits.replace(' ', '')

    length = len(digits)

    if length < 6:
        DateTrue = 'False'
        return DateTrue

    if  DateTrue != 'False':
        DateTrue = digits
        return DateTrue

def date_check(barcodeTrue, DateTrue):
    DateCheck = 'False'
    if barcodeTrue == DateTrue:
        DateCheck = 'True'
        return DateCheck
    bar = barcodeTrue.replace('-','')
    date = DateTrue.replace('-','')
    if bar == date:
        DateCheck = 'True'
        return DateCheck
    lengthD = len(date)
    lengthB = len(bar)
    if lengthD == 10:
        date0 = date[:4]+date[5:7]+date[lengthD-2:]
        if bar == date0:
            DateCheck = 'True'
            return DateCheck
        else:
            count = 0
            charCount = 0
            for Bchar in bar:
                if Bchar == date[charCount]:
                    count += 1
                    charCount += 1
                    continue
                if Bchar == date[charCount+1] and charCount != 9:
                    count += 1
                    charCount += 2
                else:
                    charCount += 1
        if count > 6:
            DateCheck = 'True*'
    if lengthD == 9:
        date1 = date[:6]+date[lengthD-2:]
        if bar == date1:
            DateCheck = 'True'
            return DateCheck
        date1 = date[:4]+date[5:]
        if bar == date1:
            DateCheck = 'True'
            return DateCheck
        else:
            count = 0
            charCount = 0
            for Bchar in bar:
                d = date[charCount]
                if Bchar == date[charCount]:
                    count += 1
                    charCount += 1
                    continue
                d = date[charCount + 1]
                if Bchar == date[charCount+1] and charCount != 8:
                    count += 1
                    charCount += 2
                else:
                    charCount += 1
        if count > 6:
            DateCheck = 'True*'
    date2 = date[:2]+date[3:5]+date[lengthD-2:]
    bar2 = bar[:2]+bar[4:6]+bar[lengthB-2:]
    if bar2 == date2:
        DateCheck = 'True'
        return DateCheck
    date3 = date[:4]+date[5]+date[lengthD-2:]
    bar3 = bar[:4]+bar[5]+date[lengthB-2:]
    if bar3 == date3:
        DateCheck = 'True'
        return DateCheck
    return DateCheck

def Barcode_Validate(Barcode):
    # load the image and convert it to grayscale
    image = Barcode
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    # compute the Scharr gradient magnitude representation of the images in both the x and y direction using OpenCV 2.4
    ddepth = cv2.cv.CV_32F if imutils.is_cv2() else cv2.CV_32F
    gradX = cv2.Sobel(gray, ddepth=ddepth, dx=1, dy=0, ksize=-1)
    gradY = cv2.Sobel(gray, ddepth=ddepth, dx=0, dy=1, ksize=-1)

    # subtract the y-gradient from the x-gradient
    gradient = cv2.subtract(gradX, gradY)
    gradient = cv2.convertScaleAbs(gradient)

    # blur and threshold the image
    blurred = cv2.blur(gradient, (5, 5))
    (_, thresh) = cv2.threshold(blurred, 225, 255, cv2.THRESH_BINARY)

    # construct a closing kernel and apply it to the thresholded image
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (21, 7))
    closed = cv2.morphologyEx(thresh, cv2.MORPH_CLOSE, kernel)

    # perform a series of erosions and dilations
    closed = cv2.erode(closed, None, iterations = 4)
    closed = cv2.dilate(closed, None, iterations = 4)

    # find the contours in the thresholded image, then sort the contours by their area, keeping only the largest one
    cnts = cv2.findContours(closed.copy(), cv2.RETR_EXTERNAL,
        cv2.CHAIN_APPROX_SIMPLE)
    cnts = cnts[0] if imutils.is_cv2() else cnts[1]
    if cnts != []:
        c = sorted(cnts, key = cv2.contourArea, reverse = True)[0]

        count = 0
        for each in c:
            for one in each:
                count += 1
        
        if count >= 30:
            return 'Barcode Found'

    return 'Barcode Not Found'

def LastThree_Validate(LastThreeImg, type):
    digits = digits_extract(LastThreeImg, type)
    # find length of digits string
    length = len(digits)
    # Move onto the next one if cannot find any digits
    if length == 0 or length == 1:
        result = 'Could not Match'
        return result
    matchspace = re.search(r' ', digits)
    matchdot = re.search(r'.', digits)
    matchdash = re.search(r'-', digits)
    if (matchspace is not None) or (matchdot is not None) or (matchdash is not None):
        digits = digits.replace(' ', '')
        digits = digits.replace('.', '')
        digits = digits.replace('-', '')


    #Remove anything in second line
    match = re.search(r'\n', digits, re.M)
    if match is not None:
        digits = digits[:match.regs[0][0]]
        length = len(digits)
        if length == 0 or length == 1:
            result = 'Could not Match'
            return result

    #Make sure that we are focusing on the right second last digit
    length = len(digits)
    thirdlast = digits[(length - 3)]
    if thirdlast == '3':
        thirdlast = '0'
    if (thirdlast == '0') or (thirdlast == '1'):
        secondLast = digits[length - 2]
    else:
        secondLast = digits[(length - 1)]

    # Change wrong ocr'd digits (3,5) with 8
    if (secondLast == '3') or (secondLast == '5') or (secondLast == '9') or (secondLast == '2'):
        pos = 0
        newDigits = ''
        while pos is not length:
            digits2 = digits[pos]
            if pos == length - 2:
                digits2 = '8'
            newDigits = newDigits + digits2
            pos = pos + 1
        digits = newDigits
        secondLast = '8'

    if secondLast == '8':
        result = 'SouthAfrican'
        return result
    else:
        result = 'Foreign'
        return result
        
Images = []
ImagesPath = 'All/'
for file in os.listdir(ImagesPath):
    if file.endswith(".png"):
        Images.append(os.path.join(ImagesPath, file))

numbers = [0,0,0,0,0,0,0,0,0]
for imag in Images:
    # Initialize strings
    barcodeTrue = ''
    DateTrue = ''

    # Initialize the list variables
    RoIs = []
    Threshed = []
    jsonText = []
    files = []
    RoICompareResult = []
    templates = []
    finalResultSet = []
    #Read in image in grayscale
    imgLoad = cv2.imread(imag)

    #find shape of image
    (h, w) = imgLoad.shape[:2]

    #Extract and Write DoIs
    img = imgLoad[round(int(h)*RoIDim[0]) : round(int(h)*RoIDim[1]), round(int(w)*RoIDim[2]) : round(int(w)*RoIDim[3])]

    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    #Convert image to treshold image
    ret, img = cv2.threshold(img, 100, 255, cv2.THRESH_BINARY)

    #find shape of ROI
    (h, w) = img.shape[:2]

    img2 = np.zeros((h,w,3), np.uint8)
    img2[0:h,0:w] = (255,255,255)
    img2 = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)

    ret, img2 = cv2.threshold(img2, 100, 255, cv2.THRESH_BINARY)

    img2 = cv2.resize(img2, (w, h))

    (score, diff) = compare_ssim(img, img2, full=True, multichannel=True)
    diff = (diff * 255).astype("uint8")

    # check to see which category they fit into
    # above a 90% threshold is Type 1
    if score > 0.9:
        ROIs = Extract_ROIs(imgLoad, 0)
        Threshed = ROI_Thresh(ROIs, 0)
        Emblemsfound = find_and_validate_emblems(imgLoad)
        Barcode, DateCheck, LastThreeResult = Date_Nationality_Validation(imgLoad, Threshed[2], 0)
        Barcodefound = Barcode_Validate(Barcode)
    # below a 90% threshold is Type 2
    else:
        ROIs = Extract_ROIs(imgLoad, 1)
        Threshed = ROI_Thresh(ROIs, 1)
        Emblemsfound = find_and_validate_emblems(imgLoad)
        Barcode, DateCheck, LastThreeResult = Date_Nationality_Validation(imgLoad, Threshed[2], 1)
        Barcodefound = Barcode_Validate(Barcode)

    # finalResultSet.append(['RoIMatching', str(ROITrue)])
    finalResultSet.append(['EmblemsFound', str(Emblemsfound)])
    finalResultSet.append(['DateCheck', DateCheck])
    finalResultSet.append(['NationalityCheck', LastThreeResult])
    finalResultSet.append(['BarcodeFound', Barcodefound])
    finalResultSet.append(['DateTime', str(datetime.datetime.now())])
    if (int(finalResultSet[0][1]) < 4) and (finalResultSet[1][1] == 'False') and (finalResultSet[2][1] == 'Could not Match') and (finalResultSet[3][1] == 'Barcode Not Found'):
        f = open('Bad.txt','a')
        f.write(imag.replace('E:\\OCR Services\\ID_Validate\\', '') + '\n')
        f.close()
        numbers[0] += 1
    if (int(finalResultSet[0][1]) < 4) and (finalResultSet[1][1] == 'False') and (finalResultSet[2][1] == 'Could not Match') and (finalResultSet[3][1] == 'Barcode Found'):
        f = open('PrettyBad.txt','a')
        f.write(imag.replace('E:\\OCR Services\\ID_Validate\\', '') + '\n')
        f.close()
        numbers[1] += 1
    if (int(finalResultSet[0][1]) == 4) and (finalResultSet[1][1] == 'False') and (finalResultSet[3][1] == 'Barcode Found'):
        f = open('Average.txt','a')
        f.write(imag.replace('E:\\OCR Services\\ID_Validate\\', '') + '\n')
        f.close()
        numbers[2] += 1
    if (int(finalResultSet[0][1]) > 2) and (finalResultSet[1][1] == 'True' or finalResultSet[1][1] == 'True*') and (finalResultSet[2][1] != 'SouthAfrican') and (finalResultSet[3][1] == 'Barcode Found'):
        f = open('Good.txt','a')
        f.write(imag.replace('E:\\OCR Services\\ID_Validate\\', '') + '\n')
        f.close()
        numbers[3] += 1
    if (int(finalResultSet[0][1]) > 2) and (finalResultSet[1][1] == 'True' or finalResultSet[1][1] == 'True*') and (finalResultSet[2][1] != 'Could not Match') and (finalResultSet[3][1] == 'Barcode Found'):
        f = open('Great.txt','a')
        f.write(imag.replace('E:\\OCR Services\\ID_Validate\\', '') + '\n')
        f.close()
        numbers[4] += 1
    if (finalResultSet[2][1] == 'Foreign'):
        f = open('Foreign.txt','a')
        f.write(imag.replace('E:\\OCR Services\\ID_Validate\\', '') + '\n')
        f.close()
        numbers[5] += 1
    if (finalResultSet[1][1] == 'False'):
        f = open('NoDates.txt','a')
        f.write(imag.replace('E:\\OCR Services\\ID_Validate\\', '') + '\n')
        f.close()
        numbers[6] += 1
    if (finalResultSet[1][1] == 'True'):
        numbers[7] += 1
    numbers[8] += 1

    Encoded_Text = json.dumps(finalResultSet)

    f = open('Log.txt','a')
    f.write(imag.replace('E:\\OCR Services\\ID_Validate\\', '') + ' - ' + Encoded_Text + '\n')
    f.close()

counter = 0
for number in numbers:
    if counter == 0:
        print('bad = ' + str(number))
        f = open('numbers.txt','a')
        f.write('bad = ' + str(number) + '\n')
        f.close()
    if counter == 1:
        print('prettybad = ' + str(number))
        f = open('numbers.txt','a')
        f.write('prettybad = ' + str(number) + '\n')
        f.close()
    if counter == 2:
        print('average = ' + str(number))
        f = open('numbers.txt','a')
        f.write('average = ' + str(number) + '\n')
        f.close()
    if counter == 3:
        print('good = ' + str(number))
        f = open('numbers.txt','a')
        f.write('good = ' + str(number) + '\n')
        f.close()
    if counter == 4:
        print('great = ' + str(number))
        f = open('numbers.txt','a')
        f.write('great = ' + str(number) + '\n')
        f.close()
    if counter == 5:
        print('foreign = ' + str(number))
        f = open('numbers.txt','a')
        f.write('foreign = ' + str(number) + '\n')
        f.close()
    if counter == 6:
        print('NoDates = ' + str(number))
        f = open('numbers.txt','a')
        f.write('NoDates = ' + str(number) + '\n')
        f.close()
    if counter == 7:
        print('Dates = ' + str(number))
        f = open('numbers.txt','a')
        f.write('Dates = ' + str(number) + '\n')
        f.close()
    if counter == 8:
        print('Total = ' + str(number))
        f = open('numbers.txt','a')
        f.write('Total = ' + str(number) + '\n')
        f.close()
    counter += 1